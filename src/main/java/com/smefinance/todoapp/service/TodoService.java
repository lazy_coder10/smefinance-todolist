package com.smefinance.todoapp.service;

import com.smefinance.todoapp.domain.Todo;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public interface TodoService {

    Todo save(Todo todo);

    List<Todo> findAll(Specification<Todo> specification);

    Optional<Todo> findOne(Long id);

    void delete(Long id);
}

package com.smefinance.todoapp.service;


import com.smefinance.todoapp.domain.TaskPriority;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public interface TaskPriorityService {

    TaskPriority save(TaskPriority taskPriority);

    List<TaskPriority> findAll();

    Optional<TaskPriority> findOne(Long id);

    void delete(Long id);
}

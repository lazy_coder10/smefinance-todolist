package com.smefinance.todoapp.service.impl;

import com.smefinance.todoapp.domain.TaskPriority;
import com.smefinance.todoapp.repository.TaskPriorityRepository;
import com.smefinance.todoapp.service.TaskPriorityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@Service
public class TaskPriorityServiceImpl implements TaskPriorityService {

    private final Logger log = LoggerFactory.getLogger(TaskPriorityServiceImpl.class);

    @Autowired
    private TaskPriorityRepository taskPriorityRepository;


    @Override
    public TaskPriority save(TaskPriority taskPriority) {
        log.debug("Request to save TaskPriority : {}", taskPriority);
        return taskPriorityRepository.save(taskPriority);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TaskPriority> findAll() {
        log.debug("Request to get all TaskPriorities");
        return taskPriorityRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TaskPriority> findOne(Long id) {
        log.debug("Request to get TaskPriority : {}", id);
        return taskPriorityRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TaskPriority : {}", id);
        taskPriorityRepository.deleteById(id);
    }
}

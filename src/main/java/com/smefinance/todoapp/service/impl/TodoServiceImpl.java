package com.smefinance.todoapp.service.impl;


import com.smefinance.todoapp.domain.Todo;
import com.smefinance.todoapp.repository.TodoRepository;
import com.smefinance.todoapp.service.TodoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TodoServiceImpl implements TodoService {

    private final Logger log = LoggerFactory.getLogger(TodoServiceImpl.class);

    private final TodoRepository todoRepository;

    public TodoServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public Todo save(Todo todo) {
        log.debug("Request to save Todo : {}", todo);
        return todoRepository.save(todo);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Todo> findAll(Specification<Todo> specification) {
        log.debug("Request to get all Todo");
        Sort sort = Sort.by(
                Sort.Order.asc("taskPriority.priorityIndex"),
                Sort.Order.desc("createdAtStmp"));
        return todoRepository.findAll(Specification.where(specification),sort);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Todo> findOne(Long id) {
        log.debug("Request to get Todo : {}", id);
        return todoRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Todo : {}", id);
        todoRepository.deleteById(id);
    }
}

package com.smefinance.todoapp.web.response;

import lombok.Data;


@Data
public class SuccessResponse<T> {
    public boolean success = true;
    public T replyMessage;

    public SuccessResponse() {
        this.success = true;
    }
    public SuccessResponse(T message) {
        this.success = true;
        this.replyMessage = message;
    }
}

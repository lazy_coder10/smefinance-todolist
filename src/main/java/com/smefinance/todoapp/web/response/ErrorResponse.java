package com.smefinance.todoapp.web.response;


import lombok.Data;


@Data
public class ErrorResponse  {
    public boolean success = false;
    public String errorCode;
    public String errorMessage;


    public ErrorResponse(String errorCode, String message) {
        this.errorCode = errorCode;
        this.errorMessage = message;
    }

}

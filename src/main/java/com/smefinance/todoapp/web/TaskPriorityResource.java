package com.smefinance.todoapp.web;

import com.smefinance.todoapp.domain.TaskPriority;
import com.smefinance.todoapp.repository.TaskPriorityRepository;
import com.smefinance.todoapp.service.TaskPriorityService;
import com.smefinance.todoapp.web.response.ErrorResponse;
import com.smefinance.todoapp.web.response.SuccessResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * REST controller
 */
@RestController
@RequestMapping("/api")
public class TaskPriorityResource {

    private final Logger log = LoggerFactory.getLogger(TaskPriorityResource.class);

    @Autowired
    TaskPriorityService taskPriorityService;

    @Autowired
    TaskPriorityRepository taskPriorityRepository;

    @PostMapping("/task-priorities")
    public ResponseEntity<?> createTaskPriority(@RequestBody TaskPriority taskPriority) throws URISyntaxException {
        log.debug("REST request to save TaskPriority : {}", taskPriority);
        if (taskPriority.getId() != null) {
            return ResponseEntity.ok().body(new ErrorResponse("TPRE-001","New Taskpriority can not have ID"));
        }
        TaskPriority result = taskPriorityService.save(taskPriority);
        return ResponseEntity.ok().body(new SuccessResponse<>(result));
    }

    @PutMapping("/task-priorities/{id}")
    public ResponseEntity<?> updateTaskPriority(@RequestBody TaskPriority taskPriority) throws URISyntaxException {
        log.debug("REST request to update TaskPriority : {}, {}", taskPriority);
        Long id = taskPriority.getId();
        if (id == null) {
            return ResponseEntity.ok().body(new ErrorResponse("TPRE-002","ID can not be null"));
        }
        if (!taskPriorityRepository.existsById(id)) {
            return ResponseEntity.ok().body(new ErrorResponse("TPRE-003","No entry found"));
        }

        TaskPriority result = taskPriorityService.save(taskPriority);
        return ResponseEntity.ok().body(new SuccessResponse<>(result));
    }


    @GetMapping("/task-priorities")
    public ResponseEntity<?> getAllTaskPriorities() {
        log.debug("REST request to get all TaskPriorities");
        return ResponseEntity.ok().body(new SuccessResponse<>(taskPriorityService.findAll()));
    }


    @GetMapping("/task-priorities/{id}")
    public ResponseEntity<?> getTaskPriority(@PathVariable Long id) {
        log.debug("REST request to get TaskPriority : {}", id);
        Optional<TaskPriority> taskPriority = taskPriorityService.findOne(id);
        return ResponseEntity.ok().body(new SuccessResponse<>(taskPriority));
    }

    @DeleteMapping("/task-priorities/{id}")
    public ResponseEntity<?> deleteTaskPriority(@PathVariable Long id) {
        log.debug("REST request to delete TaskPriority : {}", id);
        taskPriorityService.delete(id);
        return ResponseEntity.ok().body(new SuccessResponse<>());
    }
}

package com.smefinance.todoapp.web;

import com.sipios.springsearch.anotation.SearchSpec;
import com.smefinance.todoapp.domain.Todo;
import com.smefinance.todoapp.repository.TodoRepository;
import com.smefinance.todoapp.service.TodoService;
import com.smefinance.todoapp.web.response.ErrorResponse;
import com.smefinance.todoapp.web.response.SuccessResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Optional;

/**
 * REST controller
 */
@RestController
@RequestMapping("/api")
public class TodoResource {

    private final Logger log = LoggerFactory.getLogger(TodoResource.class);

    @Autowired
    private TodoService todoService;

    @Autowired
    private TodoRepository todoRepository;


    @RequestMapping(value = "/todo-lists", method = RequestMethod.POST)
    public ResponseEntity<?> createTodo(@RequestBody Todo todo){
        log.debug("REST request to save Todo : {}", todo);
        if (todo.getId() != null) {
            return ResponseEntity.ok().body(new ErrorResponse("TRE-001","New Task can not have ID"));
        }
        Todo result = todoService.save(todo);
        return ResponseEntity.ok().body(new SuccessResponse<>(result));
    }

    @RequestMapping(value = "/todo-lists", method = RequestMethod.PUT)
    public ResponseEntity<?> updateTodo( @RequestBody Todo todo){
        log.debug("REST request to update Todo : {}, {}", todo);
        Long id = todo.getId();
        if (id == null) {
            return ResponseEntity.ok().body(new ErrorResponse("TRE-002","ID can not be null"));
        }
        if (todoRepository.existsById(todo.getId())) {
            return ResponseEntity.ok().body(new ErrorResponse("TRE-003","No entry found"));
        }
        Todo result = todoService.save(todo);
        return ResponseEntity.ok().body(new SuccessResponse<>(result));
    }

    @RequestMapping(value = "/todo-lists", method = RequestMethod.GET)
    public ResponseEntity<?> getAllTodo(@SearchSpec Specification<Todo> specs) {
        log.debug("REST request to get all Todo");
        return ResponseEntity.ok().body(new SuccessResponse<>(todoService.findAll(specs)));
    }

    @RequestMapping(value = "/todo-lists/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getTodo(@PathVariable Long id) {
        log.debug("REST request to get Todo : {}", id);
        Optional<Todo> todo = todoService.findOne(id);
        return ResponseEntity.ok().body(new SuccessResponse<>(todo));
    }

    @RequestMapping(value = "/todo-lists/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteTodo(@PathVariable Long id) {
        log.debug("REST request to delete Todo : {}", id);
        todoService.delete(id);
        return ResponseEntity.ok().body(new SuccessResponse<>());
    }
}

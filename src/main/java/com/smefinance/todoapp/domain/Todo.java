package com.smefinance.todoapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;


@Data
@Entity
@Table(name = "todo_list")
public class Todo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "task_name")
    private String taskName;

    @Column(name = "task_description")
    private String taskDescription;

    @Column(name = "task_label")
    private String taskLabel;

    @Column(name = "created_at_stmp")
    private Long createdAtStmp = System.currentTimeMillis();

    @Column(name = "is_completed")
    private Boolean isCompleted = false;

    @ManyToOne
    private TaskPriority taskPriority;


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Todo)) {
            return false;
        }
        return id != null && id.equals(((Todo) o).id);
    }

    @Override
    public String toString() {
        return "Todo{" +
            "id=" + getId() +
            ", taskName='" + getTaskName() + "'" +
            ", taskDescription='" + getTaskDescription() + "'" +
            ", taskLabel='" + getTaskLabel() + "'" +
            ", createdAtStmp=" + getCreatedAtStmp() +
            "}";
    }
}

package com.smefinance.todoapp.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "task_priority")
public class TaskPriority implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "priority_name")
    private String priorityName;

    @Column(name = "priority_index")
    private Integer priorityIndex;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TaskPriority)) {
            return false;
        }
        return id != null && id.equals(((TaskPriority) o).id);
    }

    @Override
    public String toString() {
        return "TaskPriority{" +
            "id=" + getId() +
            ", priorityName='" + getPriorityName() + "'" +
            ", priorityIndex=" + getPriorityIndex() +
            "}";
    }
}

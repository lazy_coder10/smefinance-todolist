package com.smefinance.todoapp.repository;

import com.smefinance.todoapp.domain.TaskPriority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskPriorityRepository extends JpaRepository<TaskPriority,Long> {
}

package com.smefinance.todoapp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smefinance.todoapp.domain.Todo;
import com.smefinance.todoapp.repository.TodoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class TodoResourceUT {

    private final Logger log = LoggerFactory.getLogger(TodoResourceUT.class);

    private static final String DEFAULT_TASK_NAME = "TASKNAME";
    private static final String UPDATED_TASK_NAME = "UPDTASKNAME";

    private static final String DEFAULT_TASK_DESCRIPTION = "TASKDES";
    private static final String UPDATED_TASK_DESCRIPTION = "UPDTASKDES";

    private static final String DEFAULT_TASK_LABEL = "TASKLBL";
    private static final String UPDATED_TASK_LABEL = "UPDTASKLBL";

    private static final Long DEFAULT_CREATED_AT_STMP = 1L;
    private static final Long UPDATED_CREATED_AT_STMP = 2L;

    private static final Boolean DEFAULT_IS_COMPLETED = false;
    private static final Boolean UPDATED_IS_COMPLETED = true;

    private static final String ENTITY_API_URL = "/api/todo-lists";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";


    @Autowired
    private TodoRepository todoRepository;

    @Autowired
    private MockMvc restTodoMockMvc;

    private Todo todo;

    public static Todo createEntity() {
        Todo todo = new Todo();
        todo.setTaskName(DEFAULT_TASK_NAME);
        todo.setTaskDescription(DEFAULT_TASK_DESCRIPTION);
        todo.setTaskLabel(DEFAULT_TASK_LABEL);
        todo.setCreatedAtStmp(DEFAULT_CREATED_AT_STMP);
        return todo;
    }


    public static String convertToRequestBodyJson(Object o) throws JsonProcessingException {
        ObjectMapper jsonMapper = new ObjectMapper();
        return jsonMapper.writeValueAsString(o);
    }

    @BeforeEach
    public void initTest() {
        todo = createEntity();
    }

    @Test
    @Transactional
    void createTodo() throws Exception {
        int tableSizeBeforeCreate = todoRepository.findAll().size();

        restTodoMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(convertToRequestBodyJson(todo)))
                .andExpect(status().is2xxSuccessful());
        List<Todo> todoList = todoRepository.findAll();
        assertThat(todoList).hasSize(tableSizeBeforeCreate + 1);
        Todo testTodo = todoList.get(todoList.size() - 1);
        assertThat(testTodo.getTaskName()).isEqualTo(DEFAULT_TASK_NAME);
        assertThat(testTodo.getTaskDescription()).isEqualTo(DEFAULT_TASK_DESCRIPTION);
        assertThat(testTodo.getTaskLabel()).isEqualTo(DEFAULT_TASK_LABEL);
        assertThat(testTodo.getCreatedAtStmp()).isEqualTo(DEFAULT_CREATED_AT_STMP);
        assertThat(testTodo.getIsCompleted()).isEqualTo(DEFAULT_IS_COMPLETED);
    }

    /**
     * In every case controller will return a true status in response
     * as of design if a call reached controller then service is running fine
     * as a new task with id can't be created the table size will remain same after the api call
     */
    @Test
    @Transactional
    void createTodoWithExistingId() throws Exception {
        todo.setId(1L);
        int tableSizeBeforeCreate = todoRepository.findAll().size();

        restTodoMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(convertToRequestBodyJson(todo)))
                .andExpect(status().is2xxSuccessful());

        List<Todo> todoList = todoRepository.findAll();
        assertThat(todoList).hasSize(tableSizeBeforeCreate);
    }

    @Test
    @Transactional
    void updateTodo() throws Exception {
        todoRepository.saveAndFlush(todo);
        int tableSizeBeforeUpdate = todoRepository.findAll().size();
        Todo todoToBeUpdated = new Todo();
        todoToBeUpdated.setId(todo.getId());
        todoToBeUpdated.setTaskName(UPDATED_TASK_NAME);
        todoToBeUpdated.setTaskDescription(UPDATED_TASK_DESCRIPTION);
        todoToBeUpdated.setTaskLabel(UPDATED_TASK_LABEL);
        todoToBeUpdated.setCreatedAtStmp(UPDATED_CREATED_AT_STMP);
        todoToBeUpdated.setIsCompleted(UPDATED_IS_COMPLETED);

        restTodoMockMvc
                .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(convertToRequestBodyJson(todoToBeUpdated)))
                .andExpect(status().isOk());

        List<Todo> todoList = todoRepository.findAll();
        assertThat(todoList).hasSize(tableSizeBeforeUpdate);
        Todo testTodo = todoList.get(todoList.size() - 1);
        assertThat(testTodo.getTaskName()).isEqualTo(UPDATED_TASK_NAME);
        assertThat(testTodo.getTaskDescription()).isEqualTo(UPDATED_TASK_DESCRIPTION);
        assertThat(testTodo.getTaskLabel()).isEqualTo(UPDATED_TASK_LABEL);
        assertThat(testTodo.getCreatedAtStmp()).isEqualTo(UPDATED_CREATED_AT_STMP);
        assertThat(testTodo.getIsCompleted()).isEqualTo(UPDATED_IS_COMPLETED);

    }

    @Test
    @Transactional
    void updateTodoWithoutId() throws Exception {
        todoRepository.saveAndFlush(todo);
        int tableSizeBeforeUpdate = todoRepository.findAll().size();
        Todo todoToBeUpdated = new Todo();
        todoToBeUpdated.setTaskName(UPDATED_TASK_NAME);
        todoToBeUpdated.setTaskDescription(UPDATED_TASK_DESCRIPTION);
        todoToBeUpdated.setTaskLabel(UPDATED_TASK_LABEL);
        todoToBeUpdated.setCreatedAtStmp(UPDATED_CREATED_AT_STMP);
        todoToBeUpdated.setIsCompleted(UPDATED_IS_COMPLETED);

        restTodoMockMvc
                .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(convertToRequestBodyJson(todoToBeUpdated)))
                .andExpect(status().isOk());

        List<Todo> todoList = todoRepository.findAll();
        assertThat(todoList).hasSize(tableSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void getAllTodo() throws  Exception{
        todoRepository.saveAndFlush(todo);

        restTodoMockMvc
                .perform(get(ENTITY_API_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].[0].id").value(hasItem(todo.getId().intValue())))
                .andExpect(jsonPath("$.[*].[0].taskName").value(hasItem(DEFAULT_TASK_NAME)))
                .andExpect(jsonPath("$.[*].[0].taskDescription").value(hasItem(DEFAULT_TASK_DESCRIPTION)))
                .andExpect(jsonPath("$.[*].[0].taskLabel").value(hasItem(DEFAULT_TASK_LABEL)))
                .andExpect(jsonPath("$.[*].[0].createdAtStmp").value(hasItem(DEFAULT_CREATED_AT_STMP.intValue())))
                .andExpect(jsonPath("$.[*].[0].isCompleted").value(hasItem(DEFAULT_IS_COMPLETED.booleanValue())));
    }

    @Test
    @Transactional
    void getTodo() throws  Exception{
        todoRepository.saveAndFlush(todo);

        restTodoMockMvc
                .perform(get(ENTITY_API_URL_ID, todo.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(todo.getId().intValue())))
                .andExpect(jsonPath("$.[*].taskName").value(hasItem(DEFAULT_TASK_NAME)))
                .andExpect(jsonPath("$.[*].taskDescription").value(hasItem(DEFAULT_TASK_DESCRIPTION)))
                .andExpect(jsonPath("$.[*].taskLabel").value(hasItem(DEFAULT_TASK_LABEL)))
                .andExpect(jsonPath("$.[*].createdAtStmp").value(hasItem(DEFAULT_CREATED_AT_STMP.intValue())))
                .andExpect(jsonPath("$.[*].isCompleted").value(hasItem(DEFAULT_IS_COMPLETED.booleanValue())));
    }

    @Test
    @Transactional
    void deleteTodo() throws  Exception{
        todoRepository.saveAndFlush(todo);
        int tableSizeBeforeCreate = todoRepository.findAll().size();

        restTodoMockMvc
                .perform(delete(ENTITY_API_URL_ID, todo.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<Todo> todoListList = todoRepository.findAll();
        assertThat(todoListList).hasSize(tableSizeBeforeCreate - 1);
    }

}

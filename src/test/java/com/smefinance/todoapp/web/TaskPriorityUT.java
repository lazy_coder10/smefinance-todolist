package com.smefinance.todoapp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smefinance.todoapp.domain.TaskPriority;
import com.smefinance.todoapp.repository.TaskPriorityRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class TaskPriorityUT {

    private final Logger log = LoggerFactory.getLogger(TaskPriorityUT.class);

    private static final String DEFAULT_PRIORITY_NAME = "PRIORITYNAME";
    private static final String UPDATED_PRIORITY_NAME = "UPDPRIORITYNAME";

    private static final Integer DEFAULT_PRIORITY_INDEX = 1;
    private static final Integer UPDATED_PRIORITY_INDEX = 2;

    private static final String ENTITY_API_URL = "/api/task-priorities";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";


    @Autowired
    TaskPriorityRepository taskPriorityRepository;

    @Autowired
    MockMvc restTaskPriorityMockMvc;

    private TaskPriority taskPriority;

    public static TaskPriority createEntity() {
        TaskPriority taskPriority = new TaskPriority();
        taskPriority.setPriorityName(DEFAULT_PRIORITY_NAME);
        taskPriority.setPriorityIndex(DEFAULT_PRIORITY_INDEX);
        return taskPriority;
    }

    public static String convertToRequestBodyJson(Object o) throws JsonProcessingException {
        ObjectMapper jsonMapper = new ObjectMapper();
        return jsonMapper.writeValueAsString(o);
    }

    @BeforeEach
    public void initTest() {
        taskPriority = createEntity();
    }

    @Test
    @Transactional
    void createTaskPriority() throws Exception{
        int tableSizeBeforeCreate = taskPriorityRepository.findAll().size();

        restTaskPriorityMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(convertToRequestBodyJson(taskPriority)))
                .andExpect(status().isOk());

        List<TaskPriority> taskPriorityList = taskPriorityRepository.findAll();
        assertThat(taskPriorityList).hasSize(tableSizeBeforeCreate + 1);
        TaskPriority testTaskPriority = taskPriorityList.get(taskPriorityList.size() - 1);
        assertThat(testTaskPriority.getPriorityName()).isEqualTo(DEFAULT_PRIORITY_NAME);
        assertThat(testTaskPriority.getPriorityIndex()).isEqualTo(DEFAULT_PRIORITY_INDEX);

    }

    @Test
    @Transactional
    void createTaskPriorityWithId() throws Exception{
        taskPriority.setId(1L);
        int tableSizeBeforeCreate = taskPriorityRepository.findAll().size();

        restTaskPriorityMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(convertToRequestBodyJson(taskPriority)))
                .andExpect(status().isOk());

        assertThat(taskPriorityRepository.findAll().size()).isEqualTo(tableSizeBeforeCreate);
    }

    @Test
    @Transactional
    void updateTaskPriority() throws Exception{
        taskPriorityRepository.saveAndFlush(taskPriority);
        int tableSizeBeforeUpdate = taskPriorityRepository.findAll().size();
        TaskPriority taskPriorityToBeUpdated = new TaskPriority();
        taskPriorityToBeUpdated.setId(taskPriority.getId());
        taskPriorityToBeUpdated.setPriorityName(UPDATED_PRIORITY_NAME);
        taskPriorityToBeUpdated.setPriorityIndex(UPDATED_PRIORITY_INDEX);

        restTaskPriorityMockMvc
                .perform(put(ENTITY_API_URL_ID, taskPriority.getId()).contentType(MediaType.APPLICATION_JSON).content(convertToRequestBodyJson(taskPriorityToBeUpdated)))
                .andExpect(status().isOk());

        List<TaskPriority> taskPriorityList = taskPriorityRepository.findAll();
        assertThat(taskPriorityList.size()).isEqualTo(tableSizeBeforeUpdate);
        TaskPriority testTaskPriority = taskPriorityList.get(taskPriorityList.size() - 1);
        assertThat(testTaskPriority.getPriorityName()).isEqualTo(UPDATED_PRIORITY_NAME);
        assertThat(testTaskPriority.getPriorityIndex()).isEqualTo(UPDATED_PRIORITY_INDEX);
    }

    @Test
    @Transactional
    void updatePriorityIndexWithoutId() throws Exception{
        taskPriorityRepository.saveAndFlush(taskPriority);
        int tableSizeBeforeUpdate = taskPriorityRepository.findAll().size();
        TaskPriority taskPriorityToBeUpdated = new TaskPriority();
        taskPriorityToBeUpdated.setPriorityName(UPDATED_PRIORITY_NAME);
        taskPriorityToBeUpdated.setPriorityIndex(UPDATED_PRIORITY_INDEX);

        restTaskPriorityMockMvc
                .perform(put(ENTITY_API_URL_ID, taskPriority.getId()).contentType(MediaType.APPLICATION_JSON).content(convertToRequestBodyJson(taskPriorityToBeUpdated)))
                .andExpect(status().isOk());

        assertThat(taskPriorityRepository.findAll().size()).isEqualTo(tableSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void getAllTaskPriority() throws  Exception{
        taskPriorityRepository.saveAndFlush(taskPriority);

        restTaskPriorityMockMvc
                .perform(get(ENTITY_API_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].[*].id").value(hasItem(taskPriority.getId().intValue())))
                .andExpect(jsonPath("$.[*].[*].priorityName").value(hasItem(DEFAULT_PRIORITY_NAME)))
                .andExpect(jsonPath("$.[*].[*].priorityIndex").value(hasItem(taskPriority.getPriorityIndex().intValue())));
    }

    @Test
    @Transactional
    void getTaskPriority() throws  Exception{
        taskPriorityRepository.saveAndFlush(taskPriority);

        restTaskPriorityMockMvc
                .perform(get(ENTITY_API_URL_ID, taskPriority.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(taskPriority.getId().intValue())))
                .andExpect(jsonPath("$.[*].priorityName").value(hasItem(DEFAULT_PRIORITY_NAME)))
                .andExpect(jsonPath("$.[*].priorityIndex").value(hasItem(taskPriority.getPriorityIndex().intValue())));

    }

    @Test
    @Transactional
    void deleteTaskPriority() throws  Exception{
        taskPriorityRepository.saveAndFlush(taskPriority);
        int tableSizeBeforeCreate = taskPriorityRepository.findAll().size();

        restTaskPriorityMockMvc
                .perform(delete(ENTITY_API_URL_ID, taskPriority.getId()).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        List<TaskPriority> todoListList = taskPriorityRepository.findAll();
        assertThat(todoListList).hasSize(tableSizeBeforeCreate - 1);
    }
}

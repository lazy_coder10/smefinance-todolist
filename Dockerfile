FROM openjdk:11
COPY target/todoapp-0.0.1-SNAPSHOT.jar todoapp.jar
EXPOSE 8098
ENTRYPOINT ["java", "-jar", "todoapp.jar"]
## About The Project

This is simple todo application. Where user can add/edit todos and set priority.
User can also add/edit priority type and change it's priority matrix. 


### Built With

Following frameworks and tools are used to build this project

* Spring Boot 2+
* Swagger
* Spring Data Jpa
* HikariCp
* Liquibase


<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

* Java
* Docker
* Docker Compose
* Maven
### Run Application

To run the application please follow the following the steps


1. Clone the repository
   ```sh
   git clone https://gitlab.com/lazy_coder10/smefinance-todolist.git
   ```
2. Change Directory to project root
   ```sh
   cd smefinance-todolist/
   ```
3. Build jar with following command
   ```sh
   mvn clean verify -DskipTests //as hikari will look for a live DB connection, all hikari test cases will fail. Thats why testing is avoided. 
   ```
4. Run the Docker compose file
    ```sh
   docker-compose up -d 
   ```
### Access API Documentation
For API documentation SpringDoc with Swagger UI is used. To access the Swagger UI visit following link  
[Swagger UI](http://localhost:8098/swagger-ui.html)

#### Create Todo priority
  ```sh
    curl --location --request POST 'localhost:8098/api/task-priorities' \
    --header 'Content-Type: application/json' \
    --data-raw '{
    "priorityName":"Medium",
    "priorityIndex":2
}'
 ```
#### Create Todo 
By default created Todo is marked undone. To mark a Todo done refer to Todo done API
  ```sh
    curl --location --request POST 'localhost:8098/api/todo-lists' \
    --header 'Content-Type: application/json' \
    --data-raw '{
    "taskName":"task done 3",
    "taskDescription": "task done 3",
    "taskLabel":"task done 3",
    "taskPriority":{
        "id":102
    }
}'
 ```
#### Update Todo
  ```sh
    curl --location --request PUT 'localhost:8098/api/todo-lists' \
    --header 'Content-Type: application/json' \
    --data-raw '{
    "id": 101,
    "taskName":"task done updated",
    "taskDescription": "task done updated",
    "taskLabel":"task done updated",
    "taskPriority":{
        "id":102
    }
}'
 ```
#### Get All Todo
```sh
    curl --location --request GET 'localhost:8098/api/todo-lists'
 ```
#### Mark Todo Done
```sh
    curl --location --request PUT 'localhost:8098/api/todo-lists' \
    --header 'Content-Type: application/json' \
    --data-raw '{
            "id": 101,
            "taskName": "task done 3",
            "taskDescription": "task done 3",
            "taskLabel": "task done 3",
            "createdAtStmp": 1637664605476,
            "isCompleted": true,
            "taskPriority": {
                "id": 102,
                "priorityName": "Medium",
                "priorityIndex": 2
            }
}'
 ```

#### Get All Completed Todo
We have used Spring-search framework to filter out data. For complete reference please
visit the following link
[Spring-search](https://github.com/sipios/spring-search)
```sh
   curl --location --request GET 'localhost:8098/api/todo-lists?search=(isCompleted:true)'
 ```
## Scope of Improvement and Feature Implementation

- For updating an entity, currently we need to supply the whole object from webapp. This could
  be easily omitted by implementing partial update and PATCH mechanism.
- Test codes are not that upto the mark. As usually test codes are written by separate team
  I have limited professional exposure to automated testing. 
- Docker configuration can be improved without pre-building the jar first. Copying required compiled classes and files to container
  can be an option to avoid building jar in host machine. 
- Task might have end and start date. Related parties can be notified if the task is done. But this feature seemed out of scope to me. 













